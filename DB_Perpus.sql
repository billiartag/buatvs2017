drop table mhs cascade constraint purge;
create table mhs (
  nrp varchar2(9) constraint pk_mhs primary key,
  nama varchar2(30) constraint nn0_nama_mhs not null,
  gender varchar2(1) constraint nn1_gender_mhs not null,
  constraint ch0_gender_mhs check (gender='M' or gender='m' or gender='F' or gender='f')
);

insert into mhs values ('210210182','INDRA MARYATI','F');
insert into mhs values ('217011665','JASON LEONARD','M');
insert into mhs values ('217011667','KEVIN CHRISTIAN MULIA','M');
insert into mhs values ('217011668','PATRICK SOEBIANTORO','M');
insert into mhs values ('217011669','YOHANES','M');

drop table buku cascade constraint purge;
create table buku (
  idbuku varchar2(6) constraint pk_buku primary key,
  judul varchar2(40) constraint nn0_judul_buku not null,
  status varchar2(1) constraint nn1_status_buku not null,
  stok number(3),
  hargabeli number(10),
  constraint ch0_status_buku check (status='1' or status='0')
);

insert into buku values('B0001','Pemrograman Java','1',3,1000);
insert into buku values('B0002','Pemrograman Client Server','1',5,2000);
insert into buku values('B0003','Pemrograman Pascal','1',1,3000);
insert into buku values('B0004','Matematika 1','1',4,4000);
insert into buku values('B0005','Alpro 2','1',4,4000);
insert into buku values('B0006','Analisa Desain Berorientasi Objek','1',2,5000);
insert into buku values('B0007','Pengantar Teknologi Informasi','0',6,6000);
insert into buku values('B0008','Android Programming','1',7,7000);
insert into buku values('B0009','Matematika 2','0',1,8000);
insert into buku values('B0010','Pemrograman Java 2','1',3,1000);
insert into buku values('B0011','Pemrograman Client Server 2','1',5,2000);
insert into buku values('B0012','Pemrograman Pascal 2','1',1,3000);
insert into buku values('B0013','Matematika 3','1',4,4000);
insert into buku values('B0014','Analisa Desain Berorientasi Objek 2','1',2,5000);
insert into buku values('B0015','Pengantar Teknologi Informasi 2','0',6,6000);
insert into buku values('B0016','Android Programming 2','1',7,7000);
insert into buku values('B0017','Matematika 4','0',1,8000);
insert into buku values('B0018','Pemrograman Java 3','1',3,1000);
insert into buku values('B0019','Pemrograman Client Server 3','1',5,2000);
insert into buku values('B0020','Pemrograman Pascal 3','1',1,3000);
insert into buku values('B0021','Matematika 5','1',4,4000);
insert into buku values('B0022','Analisa Desain Berorientasi Objek 3','1',2,5000);
insert into buku values('B0023','Pengantar Teknologi Informasi 3','0',6,6000);
insert into buku values('B0024','Android Programming 3','1',7,7000);
insert into buku values('B0025','Matematika 6','0',1,8000);
insert into buku values('B0026','Pemrograman Java 4','1',3,1000);
insert into buku values('B0027','Pemrograman Client Server 4','1',5,2000);
insert into buku values('B0028','Pemrograman Pascal 4','1',1,3000);
insert into buku values('B0029','Matematika 7','1',4,4000);
insert into buku values('B0030','Analisa Desain Berorientasi Objek 4','1',2,5000);
insert into buku values('B0031','Pengantar Teknologi Informasi 4','0',6,6000);
insert into buku values('B0032','Android Programming 4','1',7,7000);
insert into buku values('B0033','Matematika 8','0',1,8000);
insert into buku values('B0034','Pemrograman Java 5','1',3,1000);
insert into buku values('B0035','Pemrograman Client Server 5','1',5,2000);
insert into buku values('B0036','Pemrograman Pascal 5','1',1,3000);
insert into buku values('B0037','Matematika 9','1',4,4000);
insert into buku values('B0038','Analisa Desain Berorientasi Objek 5','1',2,5000);
insert into buku values('B0039','Pengantar Teknologi Informasi 5','0',6,6000);
insert into buku values('B0040','Android Programming 5','1',7,7000);
insert into buku values('B0041','Matematika 10','0',1,8000);
insert into buku values('B0042','Pemrograman Java 6','1',3,1000);
insert into buku values('B0043','Pemrograman Client Server 6','1',5,2000);
insert into buku values('B0044','Pemrograman Pascal 6','1',1,3000);
insert into buku values('B0045','Matematika 11','1',4,4000);
insert into buku values('B0046','Analisa Desain Berorientasi Objek 6','1',2,5000);
insert into buku values('B0047','Pengantar Teknologi Informasi 6','0',6,6000);
insert into buku values('B0048','Android Programming 6','1',7,7000);
insert into buku values('B0049','Matematika 12','0',1,8000);

insert into buku values('B0050','Pemrograman Java 7','1',3,1000);
insert into buku values('B0051','Pemrograman Client Server 7','1',5,2000);
insert into buku values('B0052','Pemrograman Pascal 7','1',1,3000);
insert into buku values('B0053','Matematika 13','1',4,4000);
insert into buku values('B0054','Analisa Desain Berorientasi Objek 7','1',2,5000);
insert into buku values('B0055','Pengantar Teknologi Informasi 7','0',6,6000);
insert into buku values('B0056','Android Programming 7','1',7,7000);
insert into buku values('B0057','Matematika 14','0',1,8000);

insert into buku values('B0058','Pemrograman Java 8','1',3,1000);
insert into buku values('B0059','Pemrograman Client Server 8','1',5,2000);
insert into buku values('B0060','Pemrograman Pascal 8','1',1,3000);
insert into buku values('B0061','Matematika 15','1',4,4000);
insert into buku values('B0062','Analisa Desain Berorientasi Objek 8','1',2,5000);
insert into buku values('B0063','Pengantar Teknologi Informasi 8','0',6,6000);
insert into buku values('B0064','Android Programming 8','1',7,7000);
insert into buku values('B0065','Matematika 16','0',1,8000);

insert into buku values('B0066','Pemrograman Java 9','1',3,1000);
insert into buku values('B0067','Pemrograman Client Server 9','1',5,2000);
insert into buku values('B0068','Pemrograman Pascal 9','1',1,3000);
insert into buku values('B0069','Matematika 17','1',4,4000);
insert into buku values('B0070','Analisa Desain Berorientasi Objek 9','1',2,5000);
insert into buku values('B0071','Pengantar Teknologi Informasi 9','0',6,6000);
insert into buku values('B0072','Android Programming 9','1',7,7000);
insert into buku values('B0073','Matematika 18','0',1,8000);

insert into buku values('B0074','Pemrograman Java 10','1',3,1000);
insert into buku values('B0075','Pemrograman Client Server 10','1',5,2000);
insert into buku values('B0076','Pemrograman Pascal 10','1',1,3000);
insert into buku values('B0077','Matematika 19','1',4,4000);
insert into buku values('B0078','Analisa Desain Berorientasi Objek 10','1',2,5000);
insert into buku values('B0079','Pengantar Teknologi Informasi 10','0',6,6000);
insert into buku values('B0080','Android Programming 10','1',7,7000);
insert into buku values('B0081','Matematika 20','0',1,8000);

drop table hpinjam cascade constraint purge;
create table hpinjam (
  idpinjam varchar2(10) constraint pk_hpinjam primary key,
  nrp varchar2(9) constraint fk0_nrp_hpinjam references mhs(nrp),
  tglpinjam date,
  totalbuku number(3)
);
insert into hpinjam values('H0001','210210182',to_date('2019/04/26','YYYY/MM/DD'),'3');
insert into hpinjam values('H0002','217011665',to_date('2019/04/25','YYYY/MM/DD'),'4');
insert into hpinjam values('H0003','217011667',to_date('2019/04/24','YYYY/MM/DD'),'2');
insert into hpinjam values('H0004','217011668',to_date('2019/04/23','YYYY/MM/DD'),'5');
insert into hpinjam values('H0005','217011669',to_date('2019/04/22','YYYY/MM/DD'),'6');

drop table dpinjam cascade constraint purge;
create table dpinjam (
  idpinjam varchar2(10) constraint fk0_idpinjam_dpinjam references hpinjam(idpinjam) deferrable initially deferred,
  idbuku varchar2(5) constraint fk1_idbuku_dpinjam references buku(idbuku),
  tglpinjam date,
  tglkembali date,
  constraint pk_dpinjam primary key(idpinjam,idbuku)
);
insert into dpinjam values('H0001','B0001',to_date('2019/04/26','YYYY/MM/DD'),to_date('2019/05/26','YYYY/MM/DD'));
insert into dpinjam values('H0001','B0002',to_date('2019/04/26','YYYY/MM/DD'),to_date('2019/05/26','YYYY/MM/DD'));
insert into dpinjam values('H0001','B0003',to_date('2019/04/26','YYYY/MM/DD'),to_date('2019/05/26','YYYY/MM/DD'));

insert into dpinjam values('H0002','B0004',to_date('2019/04/25','YYYY/MM/DD'),to_date('2019/05/25','YYYY/MM/DD'));
insert into dpinjam values('H0002','B0005',to_date('2019/04/25','YYYY/MM/DD'),to_date('2019/05/25','YYYY/MM/DD'));
insert into dpinjam values('H0002','B0006',to_date('2019/04/25','YYYY/MM/DD'),to_date('2019/05/25','YYYY/MM/DD'));
insert into dpinjam values('H0002','B0007',to_date('2019/04/25','YYYY/MM/DD'),to_date('2019/05/25','YYYY/MM/DD'));

insert into dpinjam values('H0003','B0008',to_date('2019/04/24','YYYY/MM/DD'),to_date('2019/05/24','YYYY/MM/DD'));
insert into dpinjam values('H0003','B0009',to_date('2019/04/24','YYYY/MM/DD'),to_date('2019/05/24','YYYY/MM/DD'));

insert into dpinjam values('H0004','B0010',to_date('2019/04/23','YYYY/MM/DD'),to_date('2019/05/23','YYYY/MM/DD'));
insert into dpinjam values('H0004','B0011',to_date('2019/04/23','YYYY/MM/DD'),to_date('2019/05/23','YYYY/MM/DD'));
insert into dpinjam values('H0004','B0012',to_date('2019/04/23','YYYY/MM/DD'),to_date('2019/05/23','YYYY/MM/DD'));
insert into dpinjam values('H0004','B0013',to_date('2019/04/23','YYYY/MM/DD'),to_date('2019/05/23','YYYY/MM/DD'));
insert into dpinjam values('H0004','B0014',to_date('2019/04/23','YYYY/MM/DD'),to_date('2019/05/23','YYYY/MM/DD'));

insert into dpinjam values('H0005','B0015',to_date('2019/04/22','YYYY/MM/DD'),to_date('2019/05/22','YYYY/MM/DD'));
insert into dpinjam values('H0005','B0016',to_date('2019/04/22','YYYY/MM/DD'),to_date('2019/05/22','YYYY/MM/DD'));
insert into dpinjam values('H0005','B0017',to_date('2019/04/22','YYYY/MM/DD'),to_date('2019/05/22','YYYY/MM/DD'));
insert into dpinjam values('H0005','B0018',to_date('2019/04/22','YYYY/MM/DD'),to_date('2019/05/22','YYYY/MM/DD'));
insert into dpinjam values('H0005','B0019',to_date('2019/04/22','YYYY/MM/DD'),to_date('2019/05/22','YYYY/MM/DD'));
insert into dpinjam values('H0005','B0020',to_date('2019/04/22','YYYY/MM/DD'),to_date('2019/05/22','YYYY/MM/DD'));

commit;