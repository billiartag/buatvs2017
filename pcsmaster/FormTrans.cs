﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pcsmaster
{
    public partial class FormTrans : Form
    {
        Form1 flogin;
        public FormTrans()
        {
            InitializeComponent();
        }

        void loadIdTrans()
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = flogin.conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pAutoGenIDPinjam";

            OracleParameter pOut = new OracleParameter();
            pOut.ParameterName = "pID";
            pOut.OracleDbType = OracleDbType.Varchar2;
            pOut.Size = 10;
            pOut.Direction = ParameterDirection.Output;
            cmd.Parameters.Add(pOut);

            cmd.ExecuteNonQuery();
            textBox1.Text = pOut.Value.ToString();
        }

        OracleDataAdapter daMhs;
        DataSet dsMhs;
        void loadMhs()
        {
            daMhs = new OracleDataAdapter("SELECT NRP,NAMA from mhs ORDER BY 1",flogin.conn);
            dsMhs = new DataSet();
            daMhs.Fill(dsMhs, "mhs");

            comboBox1.DataSource = dsMhs.Tables["mhs"];
            comboBox1.DisplayMember = "NRP";
            comboBox1.ValueMember = "NAMA";
        }

        OracleDataAdapter daBuku;
        DataSet dsBuku;
        void loadBuku()
        {
            daBuku = new OracleDataAdapter("SELECT IDBUKU,Judul from buku WHERE status = '1' ORDER BY 1", flogin.conn);
            dsBuku = new DataSet();
            daBuku.Fill(dsBuku, "buku");

            comboBox2.DataSource = dsBuku.Tables["buku"];
            comboBox2.DisplayMember = "Judul";
            comboBox2.ValueMember = "IDBUKU";
        }

        OracleDataAdapter daDetail;
        DataSet dsDetail;
        void loadDetail()
        {
            daDetail = new OracleDataAdapter();
            dsDetail = new DataSet();

            OracleCommand cmd = new OracleCommand();
            cmd.Connection = flogin.conn;
            cmd.CommandText = "SELECT * FROM dpinjam where 1=2";

            OracleCommandBuilder builder = new OracleCommandBuilder(daDetail);
            daDetail.SelectCommand = cmd;

            daDetail.Fill(dsDetail);
            dataGridView1.DataSource = dsDetail.Tables[0];
        }

        private void FormTrans_Load(object sender, EventArgs e)
        {
            flogin = (Form1)this.Owner;
            loadIdTrans();
            loadMhs();
            loadBuku();
            loadDetail();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox2.Text = comboBox1.SelectedValue.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bool ada = false;

            for (int i = 0; i < dataGridView1.Rows.Count-1; i++)
            {
                if(dataGridView1.Rows[i].Cells[1].Value.ToString() == comboBox2.SelectedValue.ToString())
                {
                    ada = true;
                }
            }

            if (!ada)
            {
                DataRow drow = dsDetail.Tables[0].NewRow();
                drow[0] = textBox1.Text;
                drow[1] = comboBox2.SelectedValue.ToString();
                drow[2] = DateTime.Now;
                drow[3] = DateTime.Now;
                drow.EndEdit();

                dsDetail.Tables[0].Rows.Add(drow);
                textBox3.Text = (dataGridView1.Rows.Count - 1) + "";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OracleTransaction oTrans;
            //mulai transaksinya kita
            oTrans = flogin.conn.BeginTransaction();

            try
            {
                //JANGAN LUPA masukkan detail
                daDetail.Update(dsDetail);
                // masukkan header

                //OracleCommand cmd = new OracleCommand("INSERT INTO hpinjam VALUES('"+textBox1.Text+"',:nrp,:tgl,:total)", flogin.conn);

                OracleCommand cmd = new OracleCommand();
                cmd.Connection = flogin.conn;
                cmd.CommandText = "INSERT INTO hpinjam VALUES(:id,:nrp,:tgl,:total)";
                cmd.Parameters.Add(":id", textBox1.Text);
                cmd.Parameters.Add(":nrp", comboBox1.Text);
                cmd.Parameters.Add(":tgl", DateTime.Now);
                cmd.Parameters.Add(":total", textBox3.Text);
                cmd.ExecuteNonQuery();

                oTrans.Commit();
                MessageBox.Show("Pulang");
                dsDetail.Clear();
                FormTrans_Load(null, null);
            }catch(Exception ex)
            {
                oTrans.Rollback();
                MessageBox.Show(ex.Message.ToString());
            }
        }
    }
}
