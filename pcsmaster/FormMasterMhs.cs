﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace pcsmaster
{
    public partial class FormMasterMhs : Form
    {
        Form1 flogin;
        public FormMasterMhs()
        {
            InitializeComponent();
        }

        void isiDGV(string cari="")
        {
            OracleDataAdapter da = new OracleDataAdapter();
            if(cari == "")
            {
                da = new OracleDataAdapter("SELECT * FROM mhs ORDER BY 1",flogin.conn);
            }
            else
            {
                //boleh
                //da = new OracleDataAdapter("SELECT * FROM mhs WHERE nama like '%"+cari+"%' ORDER BY 1", flogin.conn);

                OracleCommand cmd = new OracleCommand();
                cmd.Connection = flogin.conn;
                cmd.CommandText = "SELECT * FROM mhs WHERE nama like :nama ORDER BY 1";
                cmd.Parameters.Add(":nama",'%'+cari+'%');

                da = new OracleDataAdapter();
                da.SelectCommand = cmd;
            }
            
            DataTable dt = new DataTable();
            da.Fill(dt);

            dataGridView1.DataSource = dt;
            dataGridView1.Refresh();

            comboBox2.DataSource = null;
            comboBox2.DataSource = dt;
            comboBox2.DisplayMember = "nama";
            comboBox2.ValueMember = "nrp";
        }

        private void FormMasterMhs_Load(object sender, EventArgs e)
        {
            flogin = (Form1)this.Owner;
            isiDGV();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(comboBox2.SelectedValue.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string cari = textBox3.Text;
            isiDGV(cari);

            //COBA ORACLE READER
            OracleCommand cmd = new OracleCommand("SELECT * FROM mhs WHERE nama like :nama ORDER BY 1", flogin.conn);
            cmd.Parameters.Add(":nama", '%' + cari + '%');

            OracleDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                listBox1.Items.Add(reader["nrp"]+" | "+reader.GetValue(1));
            }

            OracleCommand cmd2 = new OracleCommand("select count(nrp) FROM mhs WHERE nama like :nama ORDER BY 1",flogin.conn);
            cmd2.Parameters.Add(":nama", '%' + cari + '%');
            int jumlahmhs = Convert.ToInt32(cmd2.ExecuteScalar());
            listBox1.Items.Add("JUMLAH : " + jumlahmhs + "--------------");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OracleCommand cmd = new OracleCommand("INSERT INTO mhs values(:nrp,:nama,:gender)", flogin.conn);
            cmd.Parameters.Add(":nrp", textBox1.Text);
            cmd.Parameters.Add(new OracleParameter(
                ":nama",OracleDbType.Varchar2,100,textBox2.Text,ParameterDirection.Input
                ));
            cmd.Parameters.Add(":gender", comboBox1.SelectedItem.ToString().Substring(0, 1));

            cmd.ExecuteNonQuery();
            isiDGV();
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            string jk = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            if(jk == "M")
            {
                comboBox1.SelectedIndex = 0;
            }
            else
            {
                comboBox1.SelectedIndex = 1;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OracleCommand cmd = new OracleCommand("autogen",flogin.conn);
            // INI YANG PENTING
            cmd.CommandType = CommandType.StoredProcedure;

            OracleParameter output = new OracleParameter();
            output.Direction = ParameterDirection.ReturnValue;
            output.ParameterName = "gabungmaks";
            output.OracleDbType = OracleDbType.Varchar2;
            output.Size = 9;
            cmd.Parameters.Add(output);

            //untuk input
            cmd.Parameters.Add(new OracleParameter(
                "in1",OracleDbType.Varchar2,100,textBox2.Text,ParameterDirection.Input
                ));
            cmd.ExecuteNonQuery();

            string hasil = output.Value.ToString();
            MessageBox.Show(hasil);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            OracleCommand cmd = new OracleCommand("UPDATE mhs SET nama='"+ textBox2.Text + "',gender='" + comboBox1.SelectedItem.ToString().Substring(0, 1) + "' WHERE nrp='" + textBox1.Text + "'", flogin.conn);

            //OracleCommand cmd = new OracleCommand("UPDATE mhs SET nama=:nama,gender=:gender WHERE nrp=:nrp", flogin.conn);
            //cmd.Parameters.Add(":nama", textBox2.Text);
            //cmd.Parameters.Add(":gender", comboBox1.SelectedItem.ToString().Substring(0, 1));
            //cmd.Parameters.Add(":nrp", textBox1.Text);

            cmd.ExecuteNonQuery();
            isiDGV();
        }
    }
}
