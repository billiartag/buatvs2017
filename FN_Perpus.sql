/**
CONTOH MEMBUAT FUNCTION auto generate
*/
create or replace function autogen(in1 in VARCHAR2) return VARCHAR2
is 
hurufdepan varchar2(1);
hurufbelakang varchar2(1);
angka NUMBER(3);
gabung varchar2(2);
gabungmaks varchar2(9);
begin
	if INSTR(in1,' ',1)=0 THEN
		hurufdepan:=INITCAP(SUBSTR(in1,1,1));
		hurufbelakang:=INITCAP(SUBSTR(in1,2,1));
	ELSE
		hurufdepan:=INITCAP(SUBSTR(in1,1,1));
		hurufbelakang:=INITCAP(SUBSTR(in1,INSTR(in1,' ',1)+1,1));
	end IF;
	gabung := hurufdepan || hurufbelakang;
	select to_number(max(substr(nrp,3,7))) into angka
	from mhs
	where SUBSTR(nrp,1,2)=gabung;
	if angka>=0 THEN
		gabungmaks := gabung||LPAD(TO_CHAR(angka+1),7,'0');
	ELSE
		gabungmaks := gabung||LPAD(1,7,'0');
	end if;
	RETURN gabungmaks;
end;
/
show err;